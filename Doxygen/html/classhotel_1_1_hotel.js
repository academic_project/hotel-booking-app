var classhotel_1_1_hotel =
[
    [ "Hotel", "classhotel_1_1_hotel.html#a9115e991921d23039d2be1fa0fc53ab9", null ],
    [ "creerChambre", "classhotel_1_1_hotel.html#a1243688c2ffeb336a81974123e0ae006", null ],
    [ "getCasino", "classhotel_1_1_hotel.html#ae1a7abd1bc1cbf4b813f1a1c6638f13c", null ],
    [ "getChambres", "classhotel_1_1_hotel.html#ad28829f15c1307b02d53108a9518b029", null ],
    [ "getClients", "classhotel_1_1_hotel.html#a3c7940fb3b01ba2f20da1f5db1d1676a", null ],
    [ "getResto", "classhotel_1_1_hotel.html#a0041061fbfd3c4df759c5a8de0d77d6a", null ],
    [ "getSpa", "classhotel_1_1_hotel.html#a448c03474992168b870a41128fbc219f", null ],
    [ "installerClient", "classhotel_1_1_hotel.html#a08edb73c758b7ce63d0d7988d8a2f552", null ],
    [ "installerClientsdansChambres", "classhotel_1_1_hotel.html#af2daca007132d13aa07e73f79e02a5c1", null ],
    [ "prendreInfosClient", "classhotel_1_1_hotel.html#a04f8bc69381ad45d6237be4ce85c0940", null ],
    [ "setCasino", "classhotel_1_1_hotel.html#a52ce4b395a49c525091b3402772021c6", null ],
    [ "setClients", "classhotel_1_1_hotel.html#a85f25c1a51ef2e9f4d862b9387f16b81", null ],
    [ "setSpa", "classhotel_1_1_hotel.html#a603ebe357594f8da2158a97706946e67", null ]
];