var dir_4eab8a67b781ad13827222bcf2a4e1a4 =
[
    [ "Casino.java", "_casino_8java.html", [
      [ "Casino", "classhotel_1_1_casino.html", "classhotel_1_1_casino" ],
      [ "Mise", "enumhotel_1_1_casino_1_1_mise.html", "enumhotel_1_1_casino_1_1_mise" ]
    ] ],
    [ "Chambre.java", "_chambre_8java.html", [
      [ "Chambre", "classhotel_1_1_chambre.html", "classhotel_1_1_chambre" ],
      [ "Type", "enumhotel_1_1_chambre_1_1_type.html", "enumhotel_1_1_chambre_1_1_type" ]
    ] ],
    [ "Client.java", "_client_8java.html", [
      [ "Client", "classhotel_1_1_client.html", "classhotel_1_1_client" ]
    ] ],
    [ "Commande.java", "_commande_8java.html", [
      [ "Commande", "classhotel_1_1_commande.html", "classhotel_1_1_commande" ],
      [ "Formule", "enumhotel_1_1_commande_1_1_formule.html", "enumhotel_1_1_commande_1_1_formule" ]
    ] ],
    [ "Hotel.java", "_hotel_8java.html", [
      [ "Hotel", "classhotel_1_1_hotel.html", "classhotel_1_1_hotel" ]
    ] ],
    [ "Main.java", "_main_8java.html", [
      [ "Main", "classhotel_1_1_main.html", null ]
    ] ],
    [ "Restaurant.java", "_restaurant_8java.html", [
      [ "Restaurant", "classhotel_1_1_restaurant.html", "classhotel_1_1_restaurant" ]
    ] ],
    [ "Spa.java", "_spa_8java.html", [
      [ "Spa", "classhotel_1_1_spa.html", "classhotel_1_1_spa" ],
      [ "Massage", "enumhotel_1_1_spa_1_1_massage.html", "enumhotel_1_1_spa_1_1_massage" ]
    ] ],
    [ "TicketMojito.java", "_ticket_mojito_8java.html", [
      [ "TicketMojito", "classhotel_1_1_ticket_mojito.html", "classhotel_1_1_ticket_mojito" ]
    ] ]
];