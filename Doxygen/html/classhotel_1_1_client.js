var classhotel_1_1_client =
[
    [ "Client", "classhotel_1_1_client.html#a3de8e90f4f0bdf380a6eb888a96fe4b1", null ],
    [ "Client", "classhotel_1_1_client.html#ac07595d545aea78a1f4d66861e018d3e", null ],
    [ "Client", "classhotel_1_1_client.html#ab388b5cdcab577be91dd7a1c9b67457f", null ],
    [ "afficherInformationsClient", "classhotel_1_1_client.html#aac0ff0fb53ca446e314b2aba2f836271", null ],
    [ "creerCarnet", "classhotel_1_1_client.html#ac6d8279a4d53a31336178e4c317d2344", null ],
    [ "enleveTicketMojito", "classhotel_1_1_client.html#a2b0c2e30232e96b45d831e2399273f46", null ],
    [ "getArgentClient", "classhotel_1_1_client.html#ababe69c6051f4470a653e15b0ce4f9a7", null ],
    [ "getCarnetMojito", "classhotel_1_1_client.html#ab85d35fa5bb4ae5bde167a4a99275025", null ],
    [ "getCreditCasino", "classhotel_1_1_client.html#aef8de6ae7e93ea903097f9f0818bb5a8", null ],
    [ "getDuree", "classhotel_1_1_client.html#a6812418f16a03a9a8458746f14c2fa8b", null ],
    [ "getMassageElixir", "classhotel_1_1_client.html#adac91a1f192c0fe9a03cbfdf4cec3e8c", null ],
    [ "getMassagePierres", "classhotel_1_1_client.html#a21d0ad4f4f60ba547411a556d00df64f", null ],
    [ "getMassageRelax", "classhotel_1_1_client.html#a643955e76a5a49f7e19ab27aaae90b7f", null ],
    [ "getPrixApayer", "classhotel_1_1_client.html#a7861e400c8d60c86269995b60797fc61", null ],
    [ "Payer", "classhotel_1_1_client.html#aeb8934357897ea16794fdd0b15a45379", null ],
    [ "setArgentClient", "classhotel_1_1_client.html#aa0f725e2b1f944cb702ac0e75e295870", null ],
    [ "setCarnetMojito", "classhotel_1_1_client.html#a95d01b9990c097f8cb7b26f9cac71cc3", null ],
    [ "setCreditCasino", "classhotel_1_1_client.html#a0a8db20060effa75d7da3423e1c27559", null ],
    [ "setMassageElixir", "classhotel_1_1_client.html#a68cca6f1f2e80b60a9b37144ab6ce2ed", null ],
    [ "setMassagePierres", "classhotel_1_1_client.html#a80be91d67143d0cad5e956874c96ec1c", null ],
    [ "setMassageRelax", "classhotel_1_1_client.html#a8364eac04256e53f8af8e222d8e9e422", null ],
    [ "setPrixApayer", "classhotel_1_1_client.html#a22a80b0376717ce9d233eb2041b2ce70", null ],
    [ "toString", "classhotel_1_1_client.html#aebbb8ecf1e23b3e86c4c69292cd72aad", null ]
];