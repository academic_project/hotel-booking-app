package hotel;

import java.text.DateFormat;
import java.util.*;

/**
 * Cette classe représente les chambres de l'hôtel
 *
 * @author loic, yacouba
 */
public class Chambre {

    /**
     * Cette enum représente les types de chanbres de l'hôtel
     */
    protected enum Type {                       //Le type de chambre ne peuvent être que de 4 façons
        SIMPLE, DOUBLE, TRIPLE, DELUXE
    };
    private boolean Deluxe = false;             //on basculera la variable à true que si la chambre est de type Deluxe
    private String numeroChambre;
    private Type typeChambre;                   //l'attribut utilisera l'enum ci-dessus
    private Client[] client;
    private double prixChambre;                 //en euros

    /**
     * Ce constructeur concerne les chambres à une personne
     *
     * @param numeroChambree
     * @param typeChambree
     * @param clientt
     */
    public Chambre(String numeroChambree, Type typeChambree, Client clientt) {
        this.numeroChambre = numeroChambree;
        this.typeChambre = typeChambree;
        this.client = new Client[1];
        this.client[0] = clientt;
        this.prixChambre = 124.70;
        this.prixChambre += 124.70 * 0.14;                  //on renseigne le prix de la chambre
    }

    /**
     * Ce constructeur concerne les chambres à deux personnes
     *
     * @param numeroChambree
     * @param typeChambree
     * @param Deluxe
     * @param client1
     * @param client2
     */
    public Chambre(String numeroChambree, Type typeChambree, boolean Deluxe, Client client1, Client client2) {
        this.numeroChambre = numeroChambree;
        this.typeChambre = typeChambree;
        this.client = new Client[2];
        this.client[0] = client1;
        this.client[1] = client2;
        if (Deluxe) {
            this.prixChambre = 189.20;
            this.prixChambre += 189.20 * 0.14;
        } else {
            this.prixChambre = 137.60;
            this.prixChambre += 137.60 * 0.14;
        }
    }

    /**
     * Ce constructeur concerne les chambres à trois personnes
     *
     * @param numeroChambree
     * @param typeChambree
     * @param client1
     * @param client2
     * @param client3
     */
    public Chambre(String numeroChambree, Type typeChambree, Client client1, Client client2, Client client3) {
        this.numeroChambre = numeroChambree;
        this.typeChambre = typeChambree;
        this.client = new Client[3];
        this.client[0] = client1;
        this.client[1] = client2;
        this.client[2] = client3;
        this.prixChambre = 163.40;
        this.prixChambre += 163.40 * 0.14;
    }

    //Les getters et les setters de la classe
    public boolean isDeluxe() {
        return Deluxe;
    }

    public void setDeluxe(boolean Deluxe) {
        this.Deluxe = Deluxe;
    }

    public String getNumeroChambre() {
        return numeroChambre;
    }

    public void setNumeroChambre(String numeroChambre) {
        this.numeroChambre = numeroChambre;
    }

    public Type getTypeChambre() {
        return typeChambre;
    }

    public void setTypeChambre(Type typeChambre) {
        this.typeChambre = typeChambre;
    }

    public Client[] getClient() {
        return client;
    }

    public void setClient(Client[] client) {
        this.client = client;
    }

    public double getPrixChambre() {
        return prixChambre;
    }

    public void setPrixChambre(double prixChambre) {
        this.prixChambre = prixChambre;
    }

    @Override
    public String toString() {
        return "Chambre{" + "Deluxe=" + Deluxe + ", numeroChambre=" + numeroChambre + ", typeChambre=" + typeChambre + ", client=" + Arrays.toString(client) + ", prixChambre=" + prixChambre + '}';
    }

    /**
     * Cette méthode nous imprime la fiche d'une chambre comme indiqué dans
     * l'énoncé.
     */
    public void afficherInformationsChambre() {
        DateFormat mediumDateFormatEN = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, new Locale("EN", "en"));  //On récupère l'objet date associé au format que nous voulons
        System.out.println("*******************HOTEL BLUE BAY CARACAO*******************");
        System.out.println("Date de réservation : " + mediumDateFormatEN.format(new Date()));
        int i = 1;
        for (Client c : this.client) {
            System.out.println("Client " + Integer.toString(i));
            c.afficherInformationsClient();
            System.out.println(" ");
            i++;
        }
        System.out.println("Chambre No. : " + this.numeroChambre + "      Type de chambre :" + this.typeChambre);
        System.out.println("Prix (/nuit) : " + this.prixChambre);
    }

    /**
     * Cette méthode nous sert à savoir si la chambre courante est complète ou
     * pas. On utilise la méthode juste en-dessous nbreClientInstalle().
     *
     * @return
     */
    public boolean estComplet() {
        if (this.typeChambre == Type.SIMPLE && this.nbreClientInstalle() == 1) {
            return true;
        } else if ((this.typeChambre == Type.DOUBLE || this.typeChambre == Type.DELUXE) && this.nbreClientInstalle() == 2) {
            return true;
        } else if (this.typeChambre == Type.TRIPLE && this.nbreClientInstalle() == 3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cette méthode retourne le nombre de client déjà installé dans la chambre
     * qui est l'objet courant. Cette méthode semble inutile... mais si par
     * exemple nous créons des chambres vides c'est-à-dire sans mettre des vrais
     * clients dedans, ses clients seront des objets null.
     *
     * @return
     */
    public int nbreClientInstalle() {
        int cmpt = 0;
        for (Client c : this.client) {
            if (c == null) {
                return cmpt;
            }
            cmpt++;
        }
        return cmpt;
    }

    /**
     * Cette méthode simule une réservation pour une chambre simple. On
     * utilisera cette méthode dans la classe hotel pour installer les clients
     * dans leur chambre
     *
     * @param c
     */
    public void reserverSimple(Client c) {
        if (this.estComplet()) {
            System.err.println("La chambre est complète");
        } else {
            this.getClient()[0] = c;
            double res = c.getDuree() * this.getPrixChambre();
            c.setPrixApayer(res);
        }
    }

    /**
     * Cette méthode simule une réservation pour une chambre pour deux
     * personnes. On utilisera cette méthode dans la classe hotel pour installer
     * les clients dans leur chambre
     *
     * @param c1
     * @param c2
     */
    public void reserverDouble(Client c1, Client c2) {
        if (this.estComplet()) {
            System.err.println("La chambre est complète");
        } else {
            this.getClient()[0] = c1;
            this.getClient()[1] = c2;
            double res = c1.getDuree() * this.getPrixChambre();
            c1.setPrixApayer(res);
            c2.setPrixApayer(res);
        }
    }

    /**
     * Cette méthode simule une réservation pour une chambre Deluxe pour deux
     * personnes. On utilisera cette méthode dans la classe hotel pour installer
     * les clients dans leur chambre
     *
     * @param c1
     * @param c2
     */
    public void reserverDeluxe(Client c1, Client c2) {
        if (this.estComplet()) {
            System.err.println("La chambre est complète");
        } else {
            this.getClient()[0] = c1;
            this.getClient()[1] = c2;
            double res = c1.getDuree() * this.getPrixChambre();
            c1.setPrixApayer(res);
            c2.setPrixApayer(res);
        }
    }

    /**
     * Cette méthode simule une réservation pour une chambre pour trois
     * personnes.On utilisera cette méthode dans la classe hotel pour installer
     * les clients dans leur chambre
     *
     * @param c1
     * @param c2
     * @param c3
     */
    public void reserverTriple(Client c1, Client c2, Client c3) {
        if (this.estComplet()) {
            System.err.println("La chambre est complète");
        } else {
            this.getClient()[0] = c1;
            this.getClient()[1] = c2;
            this.getClient()[2] = c3;
            double res = c1.getDuree() * this.getPrixChambre();
            c1.setPrixApayer(res);
            c2.setPrixApayer(res);
            c3.setPrixApayer(res);
        }
    }

}
