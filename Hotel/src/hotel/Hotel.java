package hotel;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Cette classe représente l'hôtel
 *
 * @author loic, yacouba
 */
public class Hotel {

    private ArrayList<Chambre> chambres;     //tableau dynamique qui modélise les chambres
    private ArrayList<Client> clients;       //tableau dynamique qui modélise les clients 
    private Spa spa;                            //attribut de type spa
    private Restaurant resto;                   //attribut de type restaurant 
    private Casino casino;                      //attruibut de type casino

    /**
     * Constructeur de la classe
     *
     * @param chambres
     * @param clients
     * @param spa
     * @param resto
     * @param casino
     */
    public Hotel(ArrayList<Chambre> chambres, ArrayList<Client> clients, Spa spa, Restaurant resto, Casino casino) {
        this.chambres = chambres;
        this.clients = clients;
        this.spa = spa;
        this.resto = resto;
        this.casino = casino;
    }

    //Les getters et les setters de la classe
    public void setClients(ArrayList<Client> clients) {
        this.clients = clients;
    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public ArrayList<Chambre> getChambres() {
        return chambres;
    }

    public Restaurant getResto() {
        return resto;
    }

    public Spa getSpa() {
        return spa;
    }

    public void setSpa(Spa spa) {
        this.spa = spa;
    }

    public Casino getCasino() {
        return casino;
    }

    public void setCasino(Casino casino) {
        this.casino = casino;
    }

    /**
     * Cette méthode appelle un scanner où le client rentre ses informations on
     * simule un client qui remplit une fiche d'information à l'acceuil et enfin
     * on complète l'attribut de la classe adéquat
     */
    public void prendreInfosClient() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez rentrer vos informations svp: ");
        System.out.println("Nom: ");
        String nomClient = sc.nextLine();
        System.out.println("Prénom: ");
        String prenomClient = sc.nextLine();
        System.out.println("Date de naissance: ");
        String DateNaissance = sc.nextLine();
        System.out.println("Email: ");
        String Email = sc.nextLine();
        System.out.println("Telephone: ");
        String numTel = sc.nextLine();
        System.out.println("Indicatif (si vous voulez): ");
        String IDtel = sc.nextLine();
        System.out.println("Combien de nuits restez-vous? (Tapez un entier svp sinon cela annulera la réservation)");
        int dureeSejour = sc.nextInt();
        Client res = new Client(nomClient, prenomClient, DateNaissance, Email, numTel, IDtel, dureeSejour, null, 0, 100000);
        res.creerCarnet();
        this.clients.add(res);
    }

    /**
     * On appelle un compagnie de construction et le chantier commence en
     * construisant les premières chmabres du futur hôtel
     *
     * @param n
     * @param t
     */
    public void creerChambre(int n, Chambre.Type t) {
        for (int i = 0; i < n; i++) {
            switch (t) {
                case SIMPLE:
                    this.chambres.add(new Chambre("A" + Integer.toString(i), t, null));
                    break;
                case DOUBLE:
                    this.chambres.add(new Chambre("B" + Integer.toString(i), t, false, null, null));
                    break;
                case DELUXE:
                    this.chambres.add(new Chambre("C" + Integer.toString(i), t, true, null, null));
                    break;
                case TRIPLE:
                    this.chambres.add(new Chambre("D" + Integer.toString(i), t, null, null, null));
            }
        }
    }

    /**
     * étant donné les clients, on installe les clients dans leur chambre
     * désirée et on retourne la chambre dans laquelle on les a installés. On
     * utilise notamment cette méthode en-dessous
     *
     * @param c
     * @param t
     * @return
     */
    public Chambre installerClient(Client[] c, Chambre.Type t) {
        boolean found = true;
        int index = 0;
        for (int i = 0; found && i < this.chambres.size(); i++) {                   //on parcourt les chambres de l'hôtel (dés que found est égale a false on sort de la boucle)
            if (!(this.chambres.get(i).estComplet()) && this.chambres.get(i).getTypeChambre() == t) {       //dès qu'on trouve une chambre du type voulu qui est vide,
                found = false;                                                                              //on sort de la boucle,
            } else {
                index++;
            }                                                                                        //sinon on continue à chercher
        }
        if (index == (this.chambres.size() - 1)) {                                                          // si on n'a pas trouvé de chambres adéquates
            System.err.println("Vite!! Il faut construire une chambre de type " + t + " en plus.");
            return null;
        } else {
            switch (t) {
                case SIMPLE:
                    this.chambres.get(index).reserverSimple(c[0]);
                    break;
                case DOUBLE:
                    this.chambres.get(index).reserverDouble(c[0], c[1]);
                    break;
                case DELUXE:
                    this.chambres.get(index).reserverDeluxe(c[0], c[1]);
                    break;
                case TRIPLE:
                    this.chambres.get(index).reserverTriple(c[0], c[1], c[2]);
            }
            return this.chambres.get(index);
        }
    }

    /**
     * On appelle le groom et il amène les clients jusqu'à leur chambre On
     * envisage le cas d'un groupe d'un client (une famille) et donc il peut y
     * avoir une chambre pour les enfants et une pour les parents... Par
     * exemple, un groupe de 6 amis peut vouloir une chambre triple, une chambre
     * double/deluxe puis une chambre simple
     *
     * @param rep
     */
    public void installerClientsdansChambres(int rep) {
        Scanner sc = new Scanner(System.in);
        int clientInstalle = 0;
        int clientAInstaller = rep;
        do {
            int repp;                   //stocke le nombre de client que le groom va installer
            do {                         // Le groom ne peut pas installer plus de 3 personnes à la fois 
                System.out.println("Le groom arrive. Combien de client(s) installe-t-on? (Tapez un entier entre 1 et 3 svp)");
                repp = sc.nextInt();
            } while (repp < 1 || repp > 3 || repp > clientAInstaller);
            clientAInstaller -= repp;
            Scanner scc = new Scanner(System.in);
            String type;
            do {                         //on demande dans quelle type chambre le groom installe les clients
                System.out.println("Dans quelle type de chambre? (Tapez s pour Simple, d pour Double, de pour Deluxe, t pour triple)");
                type = scc.nextLine();
            } while (!(type.equals("s") || type.equals("d") || type.equals("de") || type.equals("t")));
            Client[] cclients = new Client[repp];
            switch (type) {
                case "s":
                    for (int i = 0; i < repp; i++) {
                        cclients[i] = this.getClients().get(i + clientInstalle);
                    }
                    Chambre c = this.installerClient(cclients, Chambre.Type.SIMPLE);
                    System.out.println("Voilà votre chambre simple. Je vous en prie installez-vous. Informations de la chambre:");
                    c.afficherInformationsChambre();
                    System.out.println(" ");
                    break;
                case "d":
                    for (int i = 0; i < repp; i++) {
                        cclients[i] = this.getClients().get(i + clientInstalle);
                    }
                    Chambre c1 = this.installerClient(cclients, Chambre.Type.DOUBLE);
                    System.out.println("Voilà votre chambre double. Je vous en prie installez-vous. Informations de la chambre:");
                    c1.afficherInformationsChambre();
                    System.out.println(" ");
                    break;
                case "de":
                    for (int i = 0; i < repp; i++) {
                        cclients[i] = this.getClients().get(i + clientInstalle);
                    }
                    Chambre c2 = this.installerClient(cclients, Chambre.Type.DELUXE);
                    System.out.println("Voilà votre chambre deluxe. Je vous en prie installez-vous. Informations de la chambre:");
                    c2.afficherInformationsChambre();
                    System.out.println(" ");
                    break;
                case "t":
                    for (int i = 0; i < repp; i++) {
                        cclients[i] = this.getClients().get(i + clientInstalle);
                    }
                    Chambre c3 = this.installerClient(cclients, Chambre.Type.TRIPLE);
                    System.out.println("Voilà votre chambre triple. Je vous en prie installez-vous. Informations de la chambre:");
                    c3.afficherInformationsChambre();
                    System.out.println(" ");
                    break;
            }
            clientInstalle += repp;
        } while (clientInstalle < rep);
    }
}
