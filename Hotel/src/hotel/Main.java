package hotel;

import java.util.*;

/**
 * La classe main simule votre séjour à l'hôtel, de l'installation dans votre
 * chambre au paiement de votre note
 *
 * @author loic, yacouba
 */
public class Main {

    /**
     * Cette méthode utilisera la sortie standard (terminal) pour simuler votre
     * séjour
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("*************Bienvenue à l'hôtel Blue Bay Caraçao (Caraïbes,Puerto Plata)***************");
        Hotel Bluebay = chantier();                                                         //on construit l'hôtel
        Restaurant resto = Bluebay.getResto();
        Spa spa = Bluebay.getSpa();
        Casino casin = Bluebay.getCasino();

        System.out.println("Salutations cher(es) client(es), je suis le directeur de l'hôtel.");
        //les clients arrivent à l'acceuil
        System.out.println("Combien de personnes êtes-vous? (Tapez un entier svp)");
        int rep = sc.nextInt();

        do {
            Bluebay.prendreInfosClient();               //Chaque client remplit une fiche d'informations.
        } while (Bluebay.getClients().size() < rep);

        Bluebay.installerClientsdansChambres(rep);      //Les clients sont installés dans leur chambre

        System.out.println("************Début de votre séjour. Nous vous souhaitons un agréable séjour !!!! ***********");
        System.out.println(" ");
        int d = Bluebay.getClients().get(0).getDuree();
        for (int i = 0; i < d; i++) {                                       //Le séjour commence et on réitère les activités (les journées) autant de fois que la durée du séjour.
            System.out.println("******Jour " + (i + 1) + "*********");
            resto = resto.debutJournee(100, 100, 100, 100);                 //On ravitaille le restaurant

            Scanner t = new Scanner(System.in);
            String repp;
            do {
                System.out.println("Le jour se lève. Voulez-vous aller manger? (o/n)");
                repp = t.nextLine();
            } while (!(repp.equals("o") || repp.equals("n")));

            if (repp.equals("o")) {
                resto.servir(Bluebay.getClients().get(0));
            }

            Scanner l = new Scanner(System.in);
            String ans;
            do {
                System.out.println("Que voulez-vous faire ensuite? (spa/casino/rien)");
                ans = l.nextLine();
            } while (!(ans.equals("spa") || ans.equals("casino") || ans.equals("rien")));

            if (ans.equals("spa")) {
                spa.masserClient(Bluebay.getClients().get(0));
            }
            if (ans.equals("casino")) {
                casin.jouerPartie(Bluebay.getClients().get(0));
            }
            if (ans.equals("rien")) {
                resto.finJournee();
            }
            System.out.println("Bonne nuit. A demain.");
        }
        System.out.println("");

        System.out.println("Nous espérons que vous avez passé un agréable séjour!!!!!!");
        System.out.println("Vous devez payer une note de " + Bluebay.getClients().get(0).getPrixApayer() + " euros!!");
    }

    /**
     * Cette méthode construit un hôtel. On appelle les constructeurs adéquat et
     * la méthode de la classe hotel pour construire les chambres
     *
     * @return l'hôtel que nous venons de construire
     */
    public static Hotel chantier() {
        ArrayList<Chambre> chambres = new ArrayList<>();
        ArrayList<Client> clients = new ArrayList<>();                    //pour l'instant on n'a pas de client
        Restaurant resto = new Restaurant(100, 100, 100, 100);               //on crée un resto par défaut
        Spa spa = new Spa(null);                                          //on crée un spa pour l'instant vide
        Casino casin = new Casino(null);                                  //on crée un casino vide
        Hotel res = new Hotel(chambres, clients, spa, resto, casin);        //constructeur de la classe hotel
        res.creerChambre(5, Chambre.Type.SIMPLE);                   //on construit 5 chambres simples
        res.creerChambre(5, Chambre.Type.DOUBLE);                   //on construit 5 chambres doubles
        res.creerChambre(5, Chambre.Type.DELUXE);                   //on construit 5 chambres deluxe
        res.creerChambre(5, Chambre.Type.TRIPLE);                   //on construit 5 chambres triples
        return res;
    }

}
