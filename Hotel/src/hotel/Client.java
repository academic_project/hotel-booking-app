package hotel;

/**
 * cette classe réprésente un client de l'hotel
 *
 * @author loic,yacouba
 */
public class Client {

    private String nom, prenom, dateDeNaissance, email, telephone, indicatifPays;       //les informations du client
    private int duree;                                                                  //en jours
    private double argentClient;                                                        //portefueille du client
    private double prixApayer = 0;                  //ceci représente la note du client qui stockera les prix de tous ces achats
    private TicketMojito[] carnetMojito;            //ceci est son carnet de bons TicketMojito
    private double creditCasino = 20;               //l'hôtel offre 20 euros de crédits casino 
    private int massageRelax = 0;                   //ces compteurs stockeront 
    private int massagePierres = 0;                 //les massages du client
    private int massageElixir = 0;

    /**
     * Ce constructeur prend en paramètres le nom et le prénom
     *
     * @param nomm
     * @param prenomm
     */
    public Client(String nomm, String prenomm) {
        this.nom = nomm;
        this.prenom = prenomm;
        if (nomm.isEmpty() || nomm.isEmpty()) {
            this.nom = "à renseigner ultérieurement";
            this.prenom = "à renseigner ultérieurement";
            System.err.println("Attention le client veut cacher son identité");
        }
    }

    /**
     * Ce constructeur prend en paramètre tous les informations adéquates pour
     * construire un client, sauf que il ne renseigne pas encore son numéro
     *
     * @param nomm
     * @param prenomm
     * @param datee
     * @param emaill
     * @param jourr //durée de son séjour
     * @param prixAPayerr //Quand on construit un client on peut lui mettre un
     * montant sur sa note.
     * @param carnett //carnet de tickets Mojito.
     * @param Argentt //Argent du client, qu'on suppose illimité.
     */
    public Client(String nomm, String prenomm, String datee, String emaill, int jourr, double prixAPayerr, TicketMojito[] carnett, double Argentt) {
        this(nomm, prenomm);
        if ((emaill.indexOf('.', emaill.indexOf("@")) != -1) && (emaill.contains("@"))) { // si on trouve un point après l'arobase et que l'email contient un arobase
            this.email = emaill;
        } else {
            System.err.println("L'email n'est pas valide");
            this.email = "Email à renseigner ultérieurement";
        }
        if (datee.matches("[0-3]+[0-9]+/[0 1]+[0-9]+/[1 2]+[0 9]+[0-9]+[0-9]+")) {  //si la date est ??/??/???? où les ? sont des chiffres compris dans les intervalles adéquats
            this.dateDeNaissance = datee;
        } else {
            System.err.println("Date de naissance renseignée incorrecte");
            this.dateDeNaissance = "Date de naissance à renseigner ultérieurement";
        }
        if (jourr >= 1) {                                               //la durée de son séjour doit être supérieure à 1
            this.duree = jourr;
        } else {
            System.err.println("Durée renseignée incorrecte");
            this.duree = 0;
        }
        if (prixAPayerr >= 0) {                                         //le montant de sa note doit être positif
            this.prixApayer = prixAPayerr;
        } else {
            this.prixApayer = 0;
        }
        this.carnetMojito = carnett;
        this.argentClient = Argentt;
    }

    /**
     * Ce contructeur prend en paramètre tous les informations pour la
     * construction d'un client
     *
     * @param nomm
     * @param prenomm
     * @param datee
     * @param emaill
     * @param telephonee
     * @param IDtelephone
     * @param jourr
     * @param carnett
     * @param prixAPayerr
     * @param Argentt
     */
    public Client(String nomm, String prenomm, String datee, String emaill, String telephonee, String IDtelephone, int jourr, TicketMojito[] carnett, double prixAPayerr, double Argentt) {
        this(nomm, prenomm, datee, emaill, jourr, prixAPayerr, carnett, Argentt);
        if ((telephonee.length() == 10) && (telephonee.matches("[0-9]*"))) {        // si l'utilisateur rentre un numéro cohérent (de longueur 10 et composé de chiffres de 0 à 9
            if (IDtelephone.isEmpty()) {                    //si l'indicatif n'est pas renseigné
                this.telephone = telephonee;
                this.indicatifPays = "";
            } else {
                this.indicatifPays = IDtelephone;
                this.telephone = IDtelephone + telephonee.substring(1);
            }
        } else {
            System.err.println("Le numéro n'est pas valide");
            this.indicatifPays = "";
            this.telephone = "Numéro de téléphone à renseigner ultérieurement";
        }
    }

    //Les getteers et les setters de la classe
    public int getDuree() {
        return duree;
    }

    public double getArgentClient() {
        return argentClient;
    }

    public void setArgentClient(double argentClient) {
        this.argentClient = argentClient;
    }

    public double getPrixApayer() {
        return prixApayer;
    }

    public void setPrixApayer(double prixApayer) {
        this.prixApayer = prixApayer;
    }

    public TicketMojito[] getCarnetMojito() {
        return carnetMojito;
    }

    public void setCarnetMojito(TicketMojito[] carnetMojito) {
        this.carnetMojito = carnetMojito;
    }

    public double getCreditCasino() {
        return creditCasino;
    }

    public void setCreditCasino(double creditCasino) {
        this.creditCasino = creditCasino;
    }

    public int getMassageRelax() {
        return massageRelax;
    }

    public void setMassageRelax(int massageRelax) {
        this.massageRelax = massageRelax;
    }

    public int getMassagePierres() {
        return massagePierres;
    }

    public void setMassagePierres(int massagePierres) {
        this.massagePierres = massagePierres;
    }

    public int getMassageElixir() {
        return massageElixir;
    }

    public void setMassageElixir(int massageElixir) {
        this.massageElixir = massageElixir;
    }

    /**
     * On crée un affichage par défaut des informations pour un objet Client
     *
     * @return la chaîne de caractères contenant les informations
     */
    @Override
    public String toString() {
        return "Client:" + "nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance=" + dateDeNaissance + ", email=" + email + ", telephone=" + telephone + ", indicatifPays=" + indicatifPays + ", duree=" + duree + ", argentClient=" + argentClient + ", prixApayer=" + prixApayer + ", carnetMojito=" + this.carnetMojito.length;
    }

    /**
     * Cette méthode affiche les informations d'un client comme c'est écrit sur
     * une fiche de chambre
     */
    public void afficherInformationsClient() {
        System.out.println("Nom et prénom du client : " + this.nom + " " + this.prenom);
        System.out.println("E-mail : " + this.email);
        String res = (this.indicatifPays.isEmpty()) ? (" ") : ("Indicatif pays : " + this.indicatifPays);       //si l'indicatif renseigné est vide, on retourne rien sinon on retourne l'indicatif
        System.out.println("Téléphone : " + res + " , " + this.telephone);
        System.out.println("Durée de séjour : " + this.duree);
        System.out.println("Note de séjour : " + this.prixApayer);
        System.out.println("Il vous reste : " + this.carnetMojito.length + " tickets Mojitos.");

    }

    /**
     * Cette méthode va créer le carnet de tickets Mojito à l'arrivée du client
     * à l'hotel. Lorsqu'on instance un client, son carnet peut être égal à
     * l'objet null et ainsi cette méthode permet d'en créer un à l'aide de la
     * classe TicketMojito.
     */
    public void creerCarnet() {
        int nbreTicket = this.duree * 2;                        //un client aura 2 tickets par jour (-> pour 2 repas)
        this.carnetMojito = new TicketMojito[nbreTicket];
        for (int i = 0; i < this.carnetMojito.length; i++) {
            this.carnetMojito[i] = new TicketMojito();            //on remplit le carnet
        }
    }

    /**
     * Il faut bien que le client puisse payer ses dettes envers l'hôtel...
     *
     * @param Espece
     */
    public void Payer(double Espece) {
        if (this.argentClient > 0) {
            this.argentClient -= Espece;
        }
    }

    /**
     * Cette méthode sert à enlever un ticket du carnet de tickets Mojito. Tout
     * simplement le carnet courant qu'on réduit de 1 élément
     */
    public void enleveTicketMojito() {
        TicketMojito[] res = new TicketMojito[this.getCarnetMojito().length - 1];   //on crée le tableau de TicketMojito avec un élément en moins
        for (int i = 0; i < res.length; i++) {
            res[i] = new TicketMojito();
        }
        this.setCarnetMojito(res);                          //On réaffecte ce carnet au client qui est en paramètre courant
    }

}
