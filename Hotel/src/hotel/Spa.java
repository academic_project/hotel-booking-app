package hotel;

import java.util.Scanner;

/**
 * Cette classe représente un spa
 *
 * @author loic, yacouba
 */
public class Spa {

    protected enum Massage {                //enum qui représente les types de massage
        RELAXANT, PIERRES, ELIXIR
    };
    private Client c;
    private Massage massageEnCours = null;

    public Spa(Client c) {
        this.c = c;
    }

    /**
     * Cette méthode effectue un massage pour un client
     *
     * @param m
     */
    public void commencerMassage(Massage m) {
        switch (m) {
            case RELAXANT:
                this.c.setMassageRelax(this.c.getMassageRelax() + 1);       //on met à jour le compteur de massage du client
                c.setPrixApayer(c.getPrixApayer() + 50);                    //on met à jour sa note
                this.massageEnCours = Massage.RELAXANT;
                break;
            case PIERRES:
                this.c.setMassagePierres(this.c.getMassagePierres() + 1);
                c.setPrixApayer(c.getPrixApayer() + 70);
                this.massageEnCours = Massage.PIERRES;
                break;
            case ELIXIR:
                this.c.setMassageElixir(this.c.getMassageElixir() + 1);
                c.setPrixApayer(c.getPrixApayer() + 80);
                this.massageEnCours = Massage.ELIXIR;
                break;
        }
    }

    /**
     * Cette méthode imprime une fiche d'informations
     */
    public void afficherInfos() {
        int res = 0;
        this.c.afficherInformationsClient();
        System.out.println("Spa ***Yuma Massage***");
        res += this.c.getMassageRelax() * 50;
        System.out.println("Nombre Massage Relaxant : " + this.c.getMassageRelax() + " x 50 euros = " + this.c.getMassageRelax() * 50);
        res += this.c.getMassagePierres() * 70;
        System.out.println("Nombre Massage aux pierres chaudes : " + this.c.getMassagePierres() + " x 70 euros = " + this.c.getMassagePierres() * 70);
        res += this.c.getMassageElixir() * 80;
        System.out.println("Nombre Massage à l'élixir de bougie : " + this.c.getMassageElixir() + " x 80 euros = " + this.c.getMassageElixir() * 80);
        System.out.println("Total des massages à payer : " + res + " euros.");
    }

    /**
     * Le client part car le massage est fini
     */
    public void finirMassage() {
        this.c = null;
        this.massageEnCours = null;
    }

    /**
     * Il faut pouvoir changer de client
     *
     * @param c
     */
    public void setC(Client c) {
        this.c = c;
    }

    /**
     * Cette mèthode nous indique ainsi si un massage est en cours
     *
     * @return
     */
    public Massage getMassageEnCours() {
        return massageEnCours;
    }

    /**
     * On appellera cette méthode dans la classe main. Elle sert à simuler
     * l'interaction entre le masseur et le client
     *
     * @param c
     */
    public void masserClient(Client c) {
        System.out.println("Bienvenue au *****Yuma Massage*****");
        this.setC(c);
        String repp;
        do {
            String rep;
            do {                                        //on demande quel type de massage le client veut
                Scanner sc = new Scanner(System.in);
                System.out.println("Quel massage voulez-vous faire? (r/p/e)");
                rep = sc.nextLine();
            } while (!(rep.equals("r") || rep.equals("p") || rep.equals("e")));

            switch (rep) {
                case "r":
                    this.commencerMassage(Massage.RELAXANT);
                    break;
                case "p":
                    this.commencerMassage(Massage.PIERRES);
                    break;
                default:
                    this.commencerMassage(Massage.ELIXIR);
                    break;
            }

            Scanner scc = new Scanner(System.in);
            System.out.println("Voulez-vous faire un autre massage? (o/n)");
            repp = scc.nextLine();
        } while (repp.equals("o"));
        this.afficherInfos();
        this.finirMassage();
    }
}
