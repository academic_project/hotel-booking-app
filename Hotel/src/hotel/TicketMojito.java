package hotel;

/**
 * Cette classe sert juste à pouvoir par la suite de créer le carnet de Ticket
 * Mojito
 *
 * @author loic, yacouba
 */
public class TicketMojito {

    private int prix;

    /**
     * Constructeur par défaut d'un ticket
     */
    public TicketMojito() {
        this.prix = 5;
    }

    /**
     * le prix d'un ticketmojito
     *
     * @return
     */
    public int getPrix() {
        return this.prix;
    }
}
