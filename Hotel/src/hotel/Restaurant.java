package hotel;

import java.util.Scanner;

/**
 * cette classe représente le restaurant de l'hôtel, elle permet de modéliser
 * les ventes de la journée.
 *
 * @author loic,yacouba
 */
public class Restaurant {

    private int capaciteAccompagnement, capacitePlat, capaciteBoisson, capaciteDessert;                     //le restaurant a une capacite quotidienne de commande
    private int nbreCmd;                        //nombre de commande de la journée
    private double benefice;                    //bénéfice de fin de journée

    /**
     * Ce constructeur crée un restaurant avec ses ressources. On appellera ce
     * constructeur a chaque début de journée.
     *
     * @param capaciteAccompagnement
     * @param capacitePlat
     * @param capaciteBoisson
     * @param capaciteDessert
     */
    public Restaurant(int capaciteAccompagnement, int capacitePlat, int capaciteBoisson, int capaciteDessert) {
        this.capaciteAccompagnement = capaciteAccompagnement;
        this.capacitePlat = capacitePlat;
        this.capaciteBoisson = capaciteBoisson;
        this.capaciteDessert = capaciteDessert;
        this.nbreCmd = 0;
        this.benefice = 0;
    }

    /**
     * Cette méthode renvoie la commande d'un client selon la formule Mojito.
     *
     * @param c le client qui commande
     * @return la commande de la formule Mojito
     */
    public Commande mojitoCommande(Client c) {
        if ((this.capacitePlat > 0) && (this.capaciteBoisson > 0)) {                //on verifie que les ressources sont suffisantes pour cette formule
            this.nbreCmd++;
            this.capacitePlat--;
            this.capaciteBoisson--;
            c.setPrixApayer(c.getPrixApayer() + 1);                                 //on met à jour la note du client, le client devra payer 1 euro 
            c.enleveTicketMojito();                                                 //on enleve un ticket Mojito du carnet du client
            Commande res = new Commande(Commande.Formule.MOJITO);
            this.benefice += res.getPrix();                                          //on met à jour les benefices de la journee
            return res;                                                             // on retourne la commande
        } else {
            System.err.println("Il ne reste plus de plat et de dessert pour aujourd'hui...Désolé!");
            return null;
        }
    }

    /**
     * Cette méthode renvoie la commande d'un client selon la formule Daiquiri
     *
     * @param c
     * @return
     */
    public Commande daiquiriCommande(Client c) {
        if ((this.capacitePlat > 0) && (this.capaciteAccompagnement > 0) && (this.capaciteBoisson > 0)) {
            this.nbreCmd++;
            this.capacitePlat--;
            this.capaciteBoisson--;
            this.capaciteAccompagnement--;
            c.setPrixApayer(c.getPrixApayer() + 3);                 //on met à jour la note du client, le client devra payer 3 euros 
            c.enleveTicketMojito();
            Commande res = new Commande(Commande.Formule.MOJITO);
            this.benefice += res.getPrix();
            return res;
        } else {
            System.err.println("Il ne reste plus de plat, de dessert, et de boisson pour aujourd'hui...Désolé!");
            return null;
        }
    }

    /**
     * Cette méthode renvoie la commande d'un client à la carte. On vérifie que
     * chaque d'abord ce que le client vaut dans sa commande en testant les
     * booléens. Puis, on crée la commande
     *
     * @param c
     * @param plat
     * @param accompagnement
     * @param dessert
     * @param boisson
     * @return
     */
    public Commande aLaCarteCommande(Client c, boolean plat, boolean accompagnement, boolean dessert, boolean boisson) {
        Commande res = new Commande(Commande.Formule.ALACARTE);
        if (plat) {
            if (this.capacitePlat > 0) {
                this.capacitePlat--;
            } else {
                System.out.println("Il ne reste plus de plat... désolé");
                plat = false;
            }
        }
        if (accompagnement) {
            if (this.capaciteAccompagnement > 0) {
                this.capaciteAccompagnement--;
            } else {
                System.out.println("Il ne reste plus de accompagnement... désolé");
                accompagnement = false;
            }
        }
        if (dessert) {
            if (this.capaciteDessert > 0) {
                this.capaciteDessert--;
            } else {
                System.out.println("Il ne reste plus de dessert... désolé");
                dessert = false;
            }
        }
        if (boisson) {
            if (this.capaciteBoisson > 0) {
                this.capaciteBoisson--;
            } else {
                System.out.println("Il ne reste plus de boisson... désolé");
                boisson = false;
            }
        }
        res.CommandeCarte(plat, accompagnement, dessert, boisson);
        this.nbreCmd++;
        c.enleveTicketMojito();                 //on enlève un ticket
        this.benefice += res.getPrix();
        double prix = res.getPrix() - 5;        //un ticket représente 5 euros 
        if (prix > 0) {
            c.setPrixApayer(c.getPrixApayer() + prix);
        }
        if (plat || accompagnement || dessert || boisson) //si au moins un des ingrédients est présent dans la commande (au moins un des arguments est égale à true)
        {
            return res;
        }
        return null;
    }

    /**
     * Cette méthode sert à préparer le restaurant pour une nouvelle journée. On
     * appellera cette méthode en début de journée.
     *
     * @param capaciteAccompagnement
     * @param capacitePlat
     * @param capaciteBoisson
     * @param capaciteDessert
     * @return
     */
    public Restaurant debutJournee(int capaciteAccompagnement, int capacitePlat, int capaciteBoisson, int capaciteDessert) {
        return new Restaurant(capaciteAccompagnement, capacitePlat, capaciteBoisson, capaciteDessert);                  //on ravitaille le restaurant
    }

    /**
     * Cette méthode fait un résumé de la journée.
     */
    public void finJournee() {
        System.out.println("Ouf! La journée est finie...");
        System.out.println("Aujourd'hui, nous avons reçu : " + this.nbreCmd);
        String res = "";
        if (this.capaciteAccompagnement == 0) {
            res += "Il faut faire le plein d'accompagnements. ";
        }
        if (this.capaciteBoisson == 0) {
            res += "Il faut faire le plein de boissons. ";
        }
        if (this.capaciteDessert == 0) {
            res += "Il faut faire le plein de desserts. ";
        }
        if (this.capacitePlat == 0) {
            res += "Il faut faire le plein de plats. ";
        }
        System.out.println(res);
        System.out.println("Bénéfice de la journée: " + this.benefice);
    }

    /**
     * Cette méthode représente l'interaction entre le serveur et le client. La
     * méthode va appeler un scanner. On appelle cette méthode exclusivement
     * dans la classe main
     *
     * @param c
     */
    public void servir(Client c) {
        String rep;
        Commande cmd;
        do {                                                    //on demande si le client veut commander à la carte
            Scanner sc = new Scanner(System.in);
            System.out.println("Voulez-vous commander à la carte ? (o/n)");
            rep = sc.nextLine();
        } while (!(rep.equals("o") || rep.equals("n")));
        if (rep.equals("o")) {
            boolean p = demander("plat");
            boolean a = demander("accompagnement");
            boolean d = demander("dessert");
            boolean b = demander("boisson");
            cmd = this.aLaCarteCommande(c, p, a, d, b);
            System.out.println("Bon appétit. Voici votre commande: " + cmd);
        } else {
            do {
                Scanner sc = new Scanner(System.in);
                System.out.println("Quelle formule voulez-vous choisir ? (d/m)");
                rep = sc.nextLine();
            } while (!(rep.equals("d") || rep.equals("m")));
            if (rep.equals("d")) {
                cmd = this.daiquiriCommande(c);
                System.out.println("Bon appétit. Voici votre commande: " + cmd);
            } else {
                cmd = this.mojitoCommande(c);
                System.out.println("Bon appétit. Voici votre commande: " + cmd);
            }
        }

    }

    /**
     * Cette méthode est exclusivement utile à la méthode ci-dessus elle sert à
     * savoir comment le client veut composer sa commande à la carte à l'aide
     * d'un scanner
     *
     * @param s
     * @return
     */
    public boolean demander(String s) {
        String rep;
        Scanner sc = new Scanner(System.in);
        System.out.println("Voulez-vous un " + s + " ? (o/n)");
        rep = sc.nextLine();
        return rep.equals("o");
    }
}
