package hotel;

import java.util.*;

/**
 * Cette classe représente un casino, plus précisément une roulette avec les
 * mises: rouge, noir, pair, impair, manque, passe.
 *
 * @author loic, yacouba
 */
public class Casino {

    protected enum Mise {                                    //une enum qui contient les mises (manque:1-18  passe:19-36)
        ROUGE, NOIR, PAIR, IMPAIR, MANQUE, PASSE
    };
    //Ce tableau aura des clés constantes correspondants aux six mises possibles, et ces valeurs seront les mises d'argent du client. Ce tableau associatif modélise notre roulette
    private HashMap<Mise, Integer> mise;

    private double gain, solde, miseDepart, deficit;                //en euros
    private Client joueur;

    /**
     * Le constructeur dépend juste du client et initialise notre tableau
     * associatif aux mises adéquates
     *
     * @param c
     */
    public Casino(Client c) {
        this.mise = new HashMap<>();
        this.mise.put(Mise.ROUGE, 0);
        this.mise.put(Mise.NOIR, 0);
        this.mise.put(Mise.PAIR, 0);
        this.mise.put(Mise.IMPAIR, 0);
        this.mise.put(Mise.MANQUE, 0);
        this.mise.put(Mise.PASSE, 0);
        this.joueur = c;
        this.gain = 0;
        this.deficit = 0;
        this.solde = 0;
        this.miseDepart = 0;
    }

    //Les getters et les setters de la classe
    public Client getJoueur() {
        return joueur;
    }

    public void setJoueur(Client joueur) {
        this.joueur = joueur;
    }

    /**
     * On suppose que lorsque le joueur commmence une session de jeu sur la
     * roulette, il mise en priorité avec ses crédits Casino et seulement
     * ensuite il puise dans son argent personnel
     *
     * @param miseDepart
     */
    public void commencerSession(int miseDepart) {
        this.miseDepart = miseDepart;
        this.solde = miseDepart;
        this.joueur.setCreditCasino(this.joueur.getCreditCasino() - miseDepart);            //on déduit les crédits casino du solde du client
        if (this.joueur.getCreditCasino() < 0) {                                            //si le client a mis une mise plus élevée que ses crédits offerts par l'hotel
            this.joueur.setArgentClient(this.joueur.getArgentClient() + this.joueur.getCreditCasino());
            this.joueur.setCreditCasino(0);
        }
    }

    /**
     * Cette méthode sert à demander au joueur si il veut jouer sur les couleurs
     * Elle va utiliser un scanner et stockera la mise du joueur dans le hashmap
     */
    public void miseCouleur() {
        Scanner sc = new Scanner(System.in);

        int miseRouge;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Rouge ? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            miseRouge = sc.nextInt();
        } while (miseRouge > this.solde);                                   //si le joueur veut miser plus que ce qu'il peut (son solde) miser, on redemande...
        this.solde -= miseRouge;                                            //on met à jour le solde
        this.deficit += miseRouge;                                           //le déficit aussi
        this.mise.replace(Mise.ROUGE, miseRouge);                           //On stocke la mise dans notre attribut

        int miseNoir;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Noir ? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            miseNoir = sc.nextInt();
        } while (miseNoir > this.solde);
        this.solde -= miseNoir;
        this.deficit += miseNoir;
        this.mise.replace(Mise.NOIR, miseNoir);
    }

    /**
     * Cette méthode sert à demander au joueur si il veut jouer sur les parites
     * Elle va utiliser un scanner et stockera la mise du joueur dans le hashmap
     */
    public void miseParite() {
        Scanner sc = new Scanner(System.in);

        int misePair;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Pair? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            misePair = sc.nextInt();
        } while (misePair > this.solde);
        this.solde -= misePair;
        this.deficit += misePair;
        this.mise.replace(Mise.PAIR, misePair);

        int miseImpair;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Impair? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            miseImpair = sc.nextInt();
        } while (miseImpair > this.solde);
        this.solde -= miseImpair;
        this.deficit += miseImpair;
        this.mise.replace(Mise.IMPAIR, miseImpair);
    }

    /**
     * Cette méthode sert à demander au joueur si il veut jouer sur manque ou
     * passe Elle va utiliser un scanner et stockera la mise du joueur dans le
     * hashmap
     */
    public void miseMaPa() {
        Scanner sc = new Scanner(System.in);

        int miseManque;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Manque? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            miseManque = sc.nextInt();
        } while (sc.nextInt() > this.solde);
        this.solde -= miseManque;
        this.deficit += miseManque;
        this.mise.replace(Mise.MANQUE, miseManque);

        int misePasse;
        do {
            System.out.println("Combien voulez-vous jouer sur Chance simple Passe? (Veuillez entrer un nombre entier)");
            System.out.println("Sachant qu'il vous reste : " + this.solde + " euros");
            misePasse = sc.nextInt();
        } while (sc.nextInt() > this.solde);
        this.solde -= misePasse;
        this.deficit += misePasse;
        this.mise.replace(Mise.PASSE, misePasse);
    }

    /**
     * On lance la bille qui va tomber sur un numéro entre 0 et 36 inclu
     *
     * @return le numéro choisi par la bille
     */
    public int lancerBille() {
        Random rand = new Random();
        int res = rand.nextInt(37);
        return res;
    }

    /**
     * Cette méthode simule une partie.
     *
     * @param c
     */
    public void jouerPartie(Client c) {
        System.out.println("Bienvenue au Casino Blue Bay Curaçao");
        this.setJoueur(c);
        Scanner sc = new Scanner(System.in);

        String rep;
        System.out.println("Veuillez rentrer une mise de départ svp (Veuillez entrer un nombre entier)");
        int miseDepartt = sc.nextInt();
        this.commencerSession(miseDepartt);                            //On commence une session de jeu
        do {
            System.out.println("Faites vos jeux:");                         // on va appeler chaque méthode pour que le joueur puisse choisir ses mises
            this.miseCouleur();
            this.miseParite();
            this.miseMaPa();
            System.out.println("Les jeux sont faits. Rien ne va plus!");

            int resultat = lancerBille();                                     // on lance la bille
            System.out.println("Il est tombé le " + resultat);

            for (Map.Entry<Mise, Integer> entry : this.mise.entrySet()) {       //on vérifie en appelant les méthodes si le joueur gagne ses mises 
                if (entry.getValue() != 0) {                                    //si la valeur est égale à 0 c'est que le joueur n'a pas misé
                    this.verifCouleur(entry.getKey(), resultat);
                    this.verifMaPa(entry.getKey(), resultat);
                    this.verifParite(entry.getKey(), resultat);
                }
            }
            this.afficherInfos();
            System.out.println("Voulez-vous continuer?[o/n]");
            Scanner scc = new Scanner(System.in);
            rep = scc.nextLine();
        } while (rep.equals("o"));                                              //tant que le joueur répond oui on retourne jouer dans la boucle...
        this.afficherInfos();
        System.out.println("Au revoir");
        this.cashOut();
    }

    /**
     * Il faut bien que le joueur puisse remettre des sous dans la machine
     *
     * @param argent
     */
    public void rechargerSolde(int argent) {
        this.solde += argent;
        this.joueur.setArgentClient(this.joueur.getArgentClient() - argent);
    }

    /**
     * Cette méthode sert à vérifier, étant donné un résultat, si la mise rouge
     * ou noir est gagné
     *
     * @param c
     * @param resultat
     */
    public void verifCouleur(Mise c, int resultat) {
        int tableauRouge[] = {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};       //tableau locale à la méthode
        int tableauNoir[] = {2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35};       //tableau locale à la méthode
        switch (c) {
            case ROUGE:
                if (resultat == 0 || contain(tableauNoir, resultat)) {                      //la méthode contain est écrite un peu plus loin
                    System.out.println("Vous n'avez pas gagné votre mise Rouge");
                } else {
                    System.out.println("Vous avez gagné votre mise Rouge");
                    this.gain += this.mise.get(Mise.ROUGE);
                    this.deficit -= this.mise.get(Mise.ROUGE);
                    this.solde += this.mise.get(Mise.ROUGE) * 2;
                }
                break;

            case NOIR:
                if (resultat == 0 || contain(tableauRouge, resultat)) {
                    System.out.println("Vous n'avez pas gagné votre mise Noir");
                } else {
                    System.out.println("Vous avez gagné votre mise Noir");
                    this.gain += this.mise.get(Mise.NOIR);
                    this.deficit -= this.mise.get(Mise.NOIR);
                    this.solde += this.mise.get(Mise.NOIR) * 2;
                }
                break;
        }
    }

    /**
     * Cette méthode sert à vérifier, étant donné un résultat, si la mise pair
     * ou impair est gagné
     *
     * @param c
     * @param resultat
     */
    public void verifParite(Mise c, int resultat) {
        switch (c) {
            case PAIR:
                if (resultat % 2 == 0) {
                    System.out.println("Vous avez gagné votre mise Pair");
                    this.gain += this.mise.get(Mise.PAIR);
                    this.deficit -= this.mise.get(Mise.PAIR);
                    this.solde += this.mise.get(Mise.PAIR) * 2;
                } else {
                    System.out.println("Vous n'avez pas gagné votre mise Pair");
                }
                break;

            case IMPAIR:
                if (resultat % 2 == 0) {
                    System.out.println("Vous n'avez pas gagné votre mise Impair");
                } else {
                    System.out.println("Vous avez gagné votre mise Impair");
                    this.gain += this.mise.get(Mise.IMPAIR);
                    this.deficit -= this.mise.get(Mise.IMPAIR);
                    this.solde += this.mise.get(Mise.IMPAIR) * 2;
                }
                break;
        }
    }

    /**
     * Cette méthode sert à vérifier, étant donné un résultat, si la mise manque
     * ou passe est gagné
     *
     * @param c
     * @param resultat
     */
    public void verifMaPa(Mise c, int resultat) {
        switch (c) {
            case MANQUE:
                if (resultat >= 1 && resultat <= 18) {
                    System.out.println("Vous avez gagné votre mise Manque");
                    this.gain += this.mise.get(Mise.MANQUE);
                    this.deficit -= this.mise.get(Mise.MANQUE);
                    this.solde += this.mise.get(Mise.MANQUE) * 2;
                } else {
                    System.out.println("Vous n'avez pas gagné votre mise Manque");
                }
                break;

            case PASSE:
                if (resultat >= 19 && resultat <= 36) {
                    System.out.println("Vous avez gagné votre mise Passe");
                    this.gain += this.mise.get(Mise.PASSE);
                    this.deficit -= this.mise.get(Mise.PASSE);
                    this.solde += this.mise.get(Mise.PASSE) * 2;
                } else {
                    System.out.println("Vous n'avez pas gagné votre mise Passe");
                }
                break;
        }
    }

    /**
     * J'utilise notamment cette méthode pour vérifier si le joueur a gagné sa
     * mise sur une des couleurs (dans Verifcouleur); qui revient à rechercher
     * si le résultat tombé est contenu dans les tableaux des chiffres rouges
     * (respectivement noirs).
     *
     * @param res
     * @param seek
     * @return
     */
    public boolean contain(int[] res, int seek) {
        boolean found = false;
        for (int i : res) {
            if (seek == i) {
                return true;
            }
        }
        return found;
    }

    /**
     * On reconfigure la roulette pour qu'elle puisse acceuillir un nouveau
     * client
     */
    public void cashOut() {
        this.joueur.setArgentClient(this.joueur.getArgentClient() + this.solde);
        this.gain = 0;
        this.solde = 0;
        this.joueur = null;
        this.mise.put(Mise.ROUGE, 0);
        this.mise.put(Mise.NOIR, 0);
        this.mise.put(Mise.PAIR, 0);
        this.mise.put(Mise.IMPAIR, 0);
        this.mise.put(Mise.MANQUE, 0);
        this.mise.put(Mise.PASSE, 0);
    }

    /**
     * Cela imprime une fiche des informations du client
     */
    public void afficherInfos() {
        System.out.println("Casino - la roulette");
        System.out.println("Mise de départ : " + this.miseDepart);
        System.out.println("Nombre de jetons inclus les 20 euros : " + this.solde);
        System.out.println("Gain : " + this.gain);
        System.out.println("Déficit : " + this.deficit);
    }
}
