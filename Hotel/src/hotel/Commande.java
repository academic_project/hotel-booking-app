package hotel;

/**
 * Cette classe sert exclusivement à la classe Restaurant car la classe aura un
 * attribut de type Commande
 *
 * @author loic, yacouba
 */
public class Commande {

    protected enum Formule {                                //la commande ne peut être égale qu'à trois formules (donc trois constantes)
        MOJITO, DAIQUIRI, ALACARTE
    };
    private boolean plat = false;
    private boolean accompagnement = false;
    private boolean dessert = false;
    private boolean boisson = false;
    private Formule formule;                                //l'attribut utilise l'enum ci-dessus
    private double prix;

    /**
     * Ce constructeur permet de créer une commande selon la formule passée en
     * paramètre; le cas d'une commande à la carte (sans formule) est traîté
     * dans une autre méthode.
     *
     * @param formule
     */
    public Commande(Formule formule) {
        this.prix = 0;
        this.formule = formule;
        if (this.formule == Formule.MOJITO) {
            this.plat = true;
            this.boisson = true;
            this.prix = 6;
        }
        if (this.formule == Formule.DAIQUIRI) {
            this.plat = true;
            this.accompagnement = true;
            this.boisson = true;
            this.prix = 8;
        }
    }

    /**
     * Cette méthode modifie les atributs de la commande passée en paramètre
     * courant afin de simuler un commande à la carte
     *
     * @param plat
     * @param accompagnement
     * @param dessert
     * @param boisson
     */
    public void CommandeCarte(boolean plat, boolean accompagnement, boolean dessert, boolean boisson) {
        if (plat) {
            this.plat = plat;
            this.prix += 5;
        }
        if (accompagnement) {
            this.accompagnement = accompagnement;
            this.prix += 2;
        }
        if (dessert) {
            this.dessert = dessert;
            this.prix += 3.5;
        }
        if (boisson) {
            this.boisson = boisson;
            this.prix += 2;
        }
    }

    /**
     * retourne le prix de la commande
     *
     * @return l'attribut de classe prix
     */
    public double getPrix() {
        return this.prix;
    }

    @Override
    public String toString() {
        return "Commande:" + "plat=" + plat + ", accompagnement=" + accompagnement + ", dessert=" + dessert + ", boisson=" + boisson + ", formule=" + formule + ", prix=" + prix;
    }

}
